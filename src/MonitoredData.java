import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class MonitoredData {
    private LocalDateTime startTime;
    private LocalDateTime endTime;
    private String activityLabel;

    public MonitoredData(LocalDateTime startTime, LocalDateTime endTime, String activityLabel) {
        this.startTime = startTime;
        this.endTime = endTime;
        this.activityLabel = activityLabel;
    }

    public String toString(){
        DateTimeFormatter date = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        return ("Start time "+date.format(startTime)+"\t End time "+date.format(endTime)+"\t Activity "+activityLabel);
    }

    public String returnMonthDayStart(){
        DateTimeFormatter date = DateTimeFormatter.ofPattern("MM dd");
        return date.format(startTime);
    }

    public String returnMonthDayEnd(){
        DateTimeFormatter date = DateTimeFormatter.ofPattern("MM dd");
        return date.format(endTime);
    }

    public String getActivityLabel(){
        return activityLabel;
    }

    public int returnStartDay(){
        DateTimeFormatter date = DateTimeFormatter.ofPattern("MMdd");
        return Integer.parseInt(date.format(startTime));
    }

    public int returnEndDay(){
        DateTimeFormatter date = DateTimeFormatter.ofPattern("MMdd");
        return Integer.parseInt(date.format(startTime));
    }

    public long duration(){
        return Duration.between(startTime,endTime).toSeconds();
    }

}
