import javax.management.monitor.Monitor;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Main {
    public static void main(String[] args) {
        List<MonitoredData> monitoredData = new ArrayList<>();
        Task task = new Task();
        try {
            monitoredData = task.read(args[0]);
        }
        catch(Exception e){
            e.printStackTrace();
        }

        try {
            task.writeTask1(monitoredData);
        }
        catch(Exception e){
            e.printStackTrace();
        }

        task.countDays(monitoredData);
        try {
            task.writeTask2(monitoredData);
        }
        catch(Exception e){
            e.printStackTrace();
        }

        Map<String,Long> activities = task.countActivitiesPeriod(monitoredData);

        try {
            task.writeTask3(activities);
        }
        catch(Exception e){
            e.printStackTrace();
        }

        Map<Integer,Map<String,Long>> activitiesDay = task.countActivitiesDay(monitoredData);

        try {
            task.writeTask4(activitiesDay);
        }
        catch(Exception e){
            e.printStackTrace();
        }

        Map<String,Long> durations = task.activityDuration(monitoredData);

        try {
            task.writeTask5(durations);
        }
        catch(Exception e){
            e.printStackTrace();
        }

        List<String> filter = task.filter(monitoredData);

        try{
            task.writeTask6(filter);
        }
        catch(Exception e){
            e.printStackTrace();
        }



    }



}
