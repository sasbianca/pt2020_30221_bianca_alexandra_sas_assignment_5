import javafx.util.Pair;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Task {

    public List<MonitoredData> read(String fileName) throws IOException {
        List<MonitoredData> monitoredData = new ArrayList<>();
        Stream<String> stream;

        stream = Files.lines(Paths.get(fileName));

        monitoredData = stream
                .map(line -> line.split("\t\t"))
                .map(line ->
                        {
                            DateTimeFormatter date = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
                            return new MonitoredData(LocalDateTime.parse(line[0],date),LocalDateTime.parse(line[1],date),line[2]);
                        }
                )
                .collect(Collectors.toList());

        return monitoredData;

    }

    public void writeTask1(List<MonitoredData> monitoredData) throws IOException {
        File file = new File("Task_1.txt");
        FileWriter fileWriter = new FileWriter(file);
        monitoredData.stream().map(Objects::toString).forEach(i -> {
            try {
                fileWriter.write(i + "\n");
            } catch (IOException e) {
                e.printStackTrace();
            }

        });
        fileWriter.close();
    }

    public long countDays(List<MonitoredData> monitoredData) {
        Stream<String> stream1, stream2;
        stream1 = monitoredData.stream().map(MonitoredData::returnMonthDayStart);
        stream2 = monitoredData.stream().map(MonitoredData::returnMonthDayEnd);
        long days = Stream.concat(stream1,stream2).distinct().count();
        return days;

    }

    public void writeTask2(List<MonitoredData> monitoredData) throws IOException {
        File file = new File("Task_2.txt");
        FileWriter fileWriter = new FileWriter(file);
        fileWriter.write(Long.toString(countDays(monitoredData)));
        fileWriter.close();

    }

    public Map<String,Long>  countActivitiesPeriod(List<MonitoredData> monitoredData) {
        Map<String,Long> activities = monitoredData.stream().collect(Collectors.groupingBy(MonitoredData::getActivityLabel, Collectors.counting()));
        return activities;
    }

    public void writeTask3(Map<String,Long> activities) throws IOException {
        File file = new File("Task_3.txt");
        FileWriter fileWriter = new FileWriter(file);

        activities.entrySet().stream().forEach(activity ->{
            try {
                fileWriter.write(activity.getKey()+" "+activity.getValue()+"\n");
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        fileWriter.close();

    }

    public Map<Integer,Map<String,Long>> countActivitiesDay(List<MonitoredData> monitoredData) {
        Map<Integer,Map<String,Long>> activities = new TreeMap<>();
        Stream<Integer> stream1, stream2;
        stream1 = monitoredData.stream().map(MonitoredData::returnStartDay);
        stream2 = monitoredData.stream().map(MonitoredData::returnEndDay);
        Stream<Integer> days = Stream.concat(stream1,stream2);

        days.forEach(day -> {
            activities.put(day,monitoredData.stream()
                    .filter(i -> i.returnStartDay() == day || i.returnEndDay() == day)
                    .collect(Collectors.groupingBy(MonitoredData::getActivityLabel,Collectors.counting())));
        });

        return activities;
    }

    public void writeTask4(Map<Integer,Map<String,Long>> activities) throws IOException {
        File file = new File("Task_4.txt");
        FileWriter fileWriter = new FileWriter(file);

        activities.entrySet().stream().forEach(activity -> {
            try {
                fileWriter.write("Day (mmdd) "+activity.getKey()+"\n");
                activity.getValue().entrySet().stream().forEach(i -> {
                    try {
                        fileWriter.write(i.getKey() + " "+ i.getValue()+"\n");
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                });
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        fileWriter.close();


    }

    public Map<String, Long> activityDuration(List<MonitoredData> monitoredData) {
        Map<String, Long> activities = monitoredData.stream().collect(Collectors.groupingBy(MonitoredData::getActivityLabel,Collectors.summingLong(MonitoredData::duration)));
        return activities;
    }

    public void writeTask5(Map<String,Long> activities) throws IOException {
        File file = new File("Task_5.txt");
        FileWriter fileWriter = new FileWriter(file);

        activities.entrySet().stream().forEach(i -> {
            try {
                fileWriter.write("Activity "+i.getKey()+" duration "+i.getValue()/3600 + " h " + (i.getValue()%3600)/60+" m "+ i.getValue()%60+" s"+"\n");
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        fileWriter.close();

    }

    public List<String> filter(List<MonitoredData> monitoredData) {
        return monitoredData.stream().map(MonitoredData::getActivityLabel).distinct().filter(i -> {
            long count = monitoredData.stream().filter(j -> j.getActivityLabel().equals(i)).count();
            long duration = monitoredData.stream().filter(j -> j.getActivityLabel().equals(i)).filter(a -> a.duration()/60 < 5).count();
            return (float)duration/count > 0.9;
        }).collect(Collectors.toList());
    }

    public void writeTask6(List<String> activities) throws IOException {
        File file = new File("Task_6.txt");
        FileWriter fileWriter = new FileWriter(file);

        activities.stream().forEach(i -> {
            try{
                fileWriter.write(i + "\n");
            }
            catch(Exception e){
                e.printStackTrace();
            }
        });

        fileWriter.close();

    }





}
